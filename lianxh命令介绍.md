
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201203101153.png)


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>



<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 开始啦！ `ssc install lianxh, replace`

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh使用介绍003.gif)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp; 

<br>
<br>


对于 **「Stata」** 这个关键词而言，微信搜索和百度搜索都太蹩脚，常常被淹没在一堆无用的信息之中。

为此，连享会团队为小伙伴们量身定制了 **lianxh** 命令 (~~纯手工打磨，宾利般的品质！~~)。

只需输入 `lianxh` 即可呈现 Stata 资源集合，包括：Stata 书籍、Blogs，论文重现网站链接，Stata Journal 单篇 PDF 等等。

输入 `lianxh DID` 即可在结果窗口中呈现包含 **DID** 关键词的所有推文，若想 **贴** 给你的微信小伙伴 (~~先别告诉她如何安装 **lianxh** 命令，否则就显不出你的无私和伟大了，坏笑~~)，只需附加一个 `w` 即可：`lianxh DID, w` 。

总之，足不出户，一切都在 **咱家里** (Stata 是我家) 搞定了！

如果时间紧迫 (~~不喜欢读太多文字~~)，那就扫一眼 **超级小抄**：

> &emsp;   
> **下载安装：** `ssc install lianxh`   
> **简介：** 搜索和呈现 Stata 常用资源和推文  
> 
> **用法：**
> - 一览无余： 
>   - 所有链接：`lianxh`
>   - 所有分类：`lianxh all`
>   - 所有推文：`lianxh +`
> - 精挑细选：
>   - **搜**：`lianxh DID`
>   - **或**：`lianxh DID RDD`
>   - **且**：`lianxh DID RDD +` (亦可写为：`lianxh DID+RDD`)     
> &emsp; 




<br>
<br>

<br>
<br>

<br>
<br>


你若是个微信分享控；又或是个爱记笔记，还会用 Markdown 写东西的进步青年，可以继续往下看……

<br>
<br>


> &emsp;   
>**进阶：** 分享和记录
>   - 微信朋友分享：附加 `weixin` 选项即可
>     - `lianxh 多期DID, w`
>   - Markdown 文本：附加 `mlink` 或 `mtext` 选项
>     - `lianxh DID, m`
>     - `lianxh DID, mt`
>   - 保存检索结果：附加 `saving(string)` 选项
>     - `lianxh DID, s(myDID)`
>     - `lianxh +, s(Blogs)`   
> &emsp; 

<br>
<br>

心动了吗？

更多功能，等你来探索！

同时，有任何建议，请联系开发团队，帮助我们做的更好 &rarr; [反馈建议](https://www.wjx.cn/jq/98072236.aspx) 。

&emsp;

> 项目主页：<https://arlionn.gitee.io/lianxh/>

<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

## GIF 动画演示

<br>
<br>


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201203111921.png)



<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 最常用的搜索： `lianxh DID`

&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh使用介绍-DID.gif)


<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 隐藏功能： `lianxh 33`

&emsp; 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh使用介绍-隐藏功能.gif)

<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 生成 Markdown 文本： `lianxh DID+倍分法, m`

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/DID_倍分法_m.gif)


<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 在微信中分享： `lianxh DID, w`

最重要的是！可以约上喜欢的人一起学习（老师再也不会担心我的学习了!）：

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/DID_w.gif)


<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> ### 哐哐哐！论文重现网站大全！  `lianxh 论文重现`

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxh使用介绍-论文重现2.gif)







&emsp;

&emsp;

&emsp;



<br>
<br>

<br>
<br>


&emsp;

> 程序小组成员：   
> &emsp; - 康峻杰 <642070192@qq.com>  
> &emsp; - 刘庆庆 <2428172451@qq.com>  
> &emsp; - 连玉君 <arlionn@163.com>   

<br>
<br>

<br>
<br>

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/20201203121959.png)



<br>
<br>

<br>
<br>


&emsp;
 
![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

> Stata连享会 &ensp; [主页](https://www.lianxh.cn/news/46917f1076104.html)  || [视频](http://lianxh.duanshu.com) || [推文](https://www.lianxh.cn/news/d4d5cd7220bc7.html) || [知乎](https://www.zhihu.com/people/arlionn/) 

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-草料主页-一码平川600.png)

&emsp;

## 1. 连享会课程

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟，[课程主页](https://gitee.com/arlionn/PanelData)
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/lianxh/Stata33)   
> - [Stata 小白的取经之路](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) - 龙志能，时长：2 小时，[课程主页](https://gitee.com/arlionn/StataBin) 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  

&emsp;

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

> ### &#x26F3; [课程主页](https://www.lianxh.cn/news/46917f1076104.html)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/Lianxh_装饰黄线.png)

&emsp;

## 2. 资源分享

### 视频公开课

- [连享会码云：100多个精选计量项目](https://www.lianxh.cn/news/944a69d75cec9.html) |  [新浪视频](https://weibo.com/tv/show/1034:4479228373303338)
- [五分钟 Markdown](https://gitee.com/arlionn/md) | [新浪视频](https://weibo.com/tv/show/1034:4484204327796746)
- [连老师给你的-听课建议](https://www.lianxh.cn/news/69706e871c9ad.html)
- [连享会 · Stata 33 讲 - 免费听](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课件](https://gitee.com/arlionn/stata101)
- [直击面板数据模型](http://lianxh-pc.duanshu.com/course/detail/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
- [Stata 33 讲](http://lianxh-pc.duanshu.com/course/detail/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. [课程主页](https://gitee.com/arlionn/stata101)
- [Stata小白的取经之路](https://gitee.com/arlionn/StataBin)，龙志能，上海财经大学，[去听课](https://lianxh.duanshu.com/#/brief/course/137d1b7c7c0045e682d3cf0cb2711530) 

### Stata

- [连享会推文](https://www.lianxh.cn) | [直播视频](http://lianxh.duanshu.com)
- **计量专题课程**: [Stata暑期班/寒假班](https://gitee.com/lianxh/text) | [专题课程](https://gitee.com/arlionn/Course)
- Stata专栏：[最新推文](https://www.lianxh.cn) | [知乎](https://www.zhihu.com/people/arlionn/) | [CSDN](https://blog.csdn.net/arlionn)
- Books and Journal: [计量Books](https://quqi.gblhgk.com/s/880197/hmpmu2ylAcvHnXwY) | [SJ-PDF](https://quqi.gblhgk.com/s/880197/eipgoUi6Gd1FDZRu) | [Stata Journal-在线浏览](https://www.lianxh.cn/news/12ffe67d8d8fb.html)
- Stata Guys：[Ben Jann](http://www.soz.unibe.ch/about_us/personen/prof_dr_jann_ben/index_eng.html) 

### Data
- [CSMAR-国泰安](http://www.gtarsc.com/#/datacenter/singletable) | [Wind-万德](https://www.wind.com.cn/Default.html) | [Resset-锐思](http://www.resset.cn/databases)
- [常用数据库](https://www.lianxh.cn/news/0b65fd5165c2c.html) 
- [人文社科开放数据库](https://www.lianxh.cn/news/6f06c914acde8.html) 
- [徐现祥教授-IRE-官员交流、方言等](https://www.lianxh.cn/news/8c9f81a5f19ee.html)
- [知乎-Data](https://www.zhihu.com/question/20179699/answer/681756635)

### Papers - 学术论文复现
- [论文重现网站](https://www.lianxh.cn/news/e87e5976686d5.html)
- [Google学术](https://ac.scmor.com/) | [统一入口：虫部落学术搜索](http://scholar.chongbuluo.com/) | [微软学术](https://academic.microsoft.com/home)
- [iData - 期刊论文下载](https://www.cn-ki.net/)
- [ CNKI ](http://scholar.cnki.net/) | [百度学术](http://xueshu.baidu.com/) | [Google学术](https://scholar.glgoo.org/) | [Sci-hub ](http://www.sci-hub.cc/), [2](http://sci-hub.ac/), [3](http://sci-hub.bz/), [4](http://sci-hub.ac/)
- Stata论文重现:  [Harvard dataverse][harvd] | [JFE][jfe]  | [github][git1] | [Yahoo-github][yahoogit]
- 学者主页(提供了诸多论文的原始数据和 dofiles)：[Angrist][Ang1] || [Daron Acemoglu][acem]  || [Ross Levine][ross] || [Esther Duflo][Duflo] || [Imbens](https://scholar.harvard.edu/imbens/software)  ||  [Raj Chetty](http://www.rajchetty.com/)

[harvd]:https://dataverse.harvard.edu/dataverse
[jfe]:http://jfe.rochester.edu/data.htm
[Ang1]:http://economics.mit.edu/faculty/angrist/data1/data
[acem]:http://economics.mit.edu/faculty/acemoglu/data
[ross]:http://faculty.haas.berkeley.edu/ross_levine/papers.htm
[duflo]:http://economics.mit.edu/faculty/eduflo/papers
[git1]:https://github.com/search?utf8=%E2%9C%93&q=stata&type=

[yahoogit]:https://search.yahoo.com/search;_ylt=AwrBT8di2LBZqyEAuG9XNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3lmcC10LTQ3MwRncHJpZAMEbl9yc2x0AzAEbl9zdWdnAzAEb3JpZ2luA3NlYXJjaC55YWhvby5jb20EcG9zAzAEcHFzdHIDBHBxc3RybAMwBHFzdHJsAzE0BHF1ZXJ5A3N0YXRhJTIwZ2l0aHViBHRfc3RtcAMxNTA0NzYxODcz?p=stata+github&fr2=sb-top&fr=yfp-t-473&fp=1

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。

&emsp; 

> &#x26F3;  **`lianxh` 命令发布了：**    
> 随时搜索连享会推文、Stata 资源，安装命令如下：  
> &emsp; `. ssc install lianxh`  
> 使用详情参见帮助文件 (有惊喜)：   
> &emsp; `. help lianxh`


&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;

